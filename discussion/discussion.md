*QUERY OPERATORS*


*COMPARISON OPERATORS*
--------------------------------------------------
*gt / $gte Operator*
db.collectionName.find({ field: { $gt: value } })
--------------------------------------------------
--------------------------------------------------
*lt / $lte Operator*
db.collectionName.find({ field: { $lt: value } })
--------------------------------------------------
--------------------------------------------------
*ne  Not Equal Operator*
db.collectionName.find({ field: { $ne: value } })
--------------------------------------------------
--------------------------------------------------
*in Operator - Specific match criteria*
db.collectionName.find({ field: { $in: value } })

*EXAMPLE 1*
db.users.find({ lastName: { $in: Hawking} })

*EXAMPLE 2*
db.users.find({ lastName: { $in: ["Hawking", "Doe"]} })
--------------------------------------------------

*LOGICAL QUERY OPERATORS*
--------------------------------------------------
*or*
db.collectionName.find({ $or: [{fieldA: valueA}, {fieldB: valueB}] })

*EXAMPLE 1*
db.users.find({ $or: [ { firstName: "Neil" }, { age: 25 } ] })

*EXAMPLE 2*
db.users.find({ $or: [ { firstName: "Neil" }, { age: { $gt: 30 } } ] })
--------------------------------------------------
*and*
db.collectionName.find({ $and: [{fieldA: valueA}, {fieldB: valueB}] })

*EXAMPLE 1*
db.users.find({ $and: [ { age: { $ne: 82 } }, { age: { $ne: 76 } } ] })
--------------------------------------------------

*EVALLUTAION QUERY OPERATOR*
--------------------------------------------------
*$regex operator* used to look for a partial match in a given field, it is used to search for STRINGS in a collection. finds only the *CASE SENSITIVE MATCH*

db.collection.find({ field: $regex: pattern/keyword })
db.collection.find({ field: $regex: 'pattern/keyword', $options: '$i' })

*EXAMPLE 1*
db.users.find({ firstName: {$regex: 'N' } })

*EXAMPLE 2 - to make matches NOT case sensitive*
db.users.find({ firstName: { $regex: 'j', $options: '$i' } })

*NOTE: $options with $i parameter specifies that we want to carry out seach without consdering the uppercase and lowercase*

*for further reference*
https://www.mongodb.com/docs/manual/reference/operator/query/regex/



----------------------
*FIELD PROJECTION*
----------------------

If you do not want to retrieve all the fields/properties of a document, you can use *field projection*

--------------
|*Inclusion* |
--------------
- include/add specific fields only when retrieving documents

- value of 1 -> to denote that the field is being included.

*SYNTAX*
db.collection.find({criteria}, {field: 1})

*EXAMPLE*
db.users.find({firstName: "Jane"}, {firstName: 1})


---------------
| *Exclusion* |
---------------
*EXAMPLE*
db.users.find({ firstName: "Jane" }, {
    
    firstName: 1,
    _id: 0
    
    })

*all info will shot but contact and department*
db.users.find({ firstName: "Jane" }, {
    
    contact: 0,
    department: 0
    
    })

*contact will only appear*
db.users.find({firstName: "Jane"}, {
    
    "contact.phone": 1,
    _id: 0
    
    })

*slicing*
db.users.find({}, {
    
    _id: 0,
    courses: { $slice: [1, 2] } 
    
    } )