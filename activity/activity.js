// 1. find users with letter s in their first name or d in their last name
//  - use the OR
//  - show only the firstName nad lastName fields and hide _id field


db.users.find({
    $or: [{firstName: {$regex: 's', $options: '$i'}}, {lastName: {$regex: 'd', $options: '$i'}} ]
}, {firstName: 1,
lastName: 1,
_id:0})


// 2. Find users who are from HR department and their age is greater than or equal to 70
//  - user AND 


db.users.find({
    $and: [{ department: "HR"}, { age: {$gte: 70}}]
})


// 3. Find users with the letter e in their first name and has an age of less than or equal to 30
//  - use AND REGEX and LTE 


db.users.find({
    $and: [{ firstName: { $regex: 'e'}}, {age: {$lte: 30}}]
})